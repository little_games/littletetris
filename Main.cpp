#include<iostream>
#include<src\Game.h>

int main(int argc, char** argv)
{
	std::cout << "Hello Tetris" << std::endl;

	Game game;

	if (game.Initialize()) game.RunLoop();

	game.Shutdown();

	return 0;
}