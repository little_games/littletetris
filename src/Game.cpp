#include "Game.h"

TetrisRules* Game::mTetrisRules = TetrisRules::GetInstances();

Game::Game()
{
	mWindow = nullptr;
	isRunning = true;
}

bool Game::Initialize()
{
	//SDL INITIALISATION
	int sdlResult = SDL_Init(SDL_INIT_EVERYTHING);

	if (sdlResult != 0)
	{
		SDL_Log("Error On SDL INITIALISATION: %s", SDL_GetError());
		return false;
	}

	//WINDOW INITIALISATION
	mWindow = SDL_CreateWindow("Little Tetris", 400, 400, mTetrisRules->GetSquareWidthSize(), mTetrisRules->GetSquareHeightSize(), 0);

	if (!mWindow)
	{
		SDL_Log("Error On Window generation: %s", SDL_GetError());
		return false;
	}

	return true;
}

void Game::RunLoop()
{
	while (isRunning)
	{
		ProcessInput();
		UpdateGame();
		GenerateOutputs();
	}
}

void Game::UpdateGame()
{

}

void Game::ProcessInput()
{

}

void Game::AddActor(class Actor* actor)
{
	mPendingActors.emplace_back(actor);
}

void Game::RemoveActor(class Actor* actor)
{
	int idToDelete = 0;
	for (int i = 0; i < mActors.size(); i++)
	{
		if (mActors[i] == actor)idToDelete = i;
	}

	mActors.erase(mActors.begin() + idToDelete);
}

void Game::UpdateActors(float deltaTime)
{
	std::vector<Actor*> actorsToDelete;

	//UPDATE ACTOR
	for (auto actor : mActors)
	{
		switch (actor->GetState())
		{
			case Actor::EActive:
				actor->update(deltaTime);
				break;

			case Actor::EDead:
				actorsToDelete.emplace_back(actor);
			break;
		}
	}

	//ADD PENDING ACTOR TO CURRENT ACTORS
	for (auto actor : mPendingActors)
	{
		mActors.emplace_back(actor);
	}
	mPendingActors.clear();

	//DELETE ACTORS
	//start to back for dodge error int
	while (!actorsToDelete.empty())
	{
		delete actorsToDelete.back();
	}

}

void Game::GenerateOutputs()
{

}

void Game::Shutdown()
{

}