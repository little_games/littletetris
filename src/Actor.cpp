#include "Actor.h"


Actor::Actor(class Game* game)
{
	mGame = game;
	mState = State::EActive;
}

void Actor::update(float delatTime)
{

}

Actor::~Actor()
{
	mGame->RemoveActor(this);
}

Actor::State Actor::GetState()
{
	return mState;
}

void Actor::SetState(Actor::State state)
{
	mState = state;
}