#pragma once
#include <SDL.h>
#include<vector>
#include<src\TetrisRules.h>
#include<src\Actor.h>



class Game
{
public:
	Game();

	//Initialize SDL Lib and all Game needed
	bool Initialize();

	//Main running loop
	void RunLoop();

	//All kill feature and memory for stop Game
	void Shutdown();

	//ACTOR FUNCTION MANAGEMENT
	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutputs();

	void UpdateActors(float deltaTime);

	//Variable
	SDL_Window *mWindow ; //Main Running window

	static TetrisRules* mTetrisRules;

	bool isRunning; //

	std::vector<class Actor*> mActors; //Active actors

	std::vector<class Actor*> mPendingActors; // Pending actor wait to integratation i


};

