#include "TetrisRules.h"

//OBLIGATOIRE QUAND UN MEMBRE EST STATIC
TetrisRules* TetrisRules::tetrisRulesInstances = nullptr;

TetrisRules::TetrisRules()
{
	squareSize = 20;
	widthSquareNumber = 10;
	heightSquareNumber = 20;
}

TetrisRules* TetrisRules::GetInstances()
{
	if (tetrisRulesInstances == nullptr) {
		tetrisRulesInstances = new TetrisRules();
	}
	return tetrisRulesInstances;
}

int TetrisRules::GetSquareSize() const
{
	return squareSize;
}

int TetrisRules::GetSquareWidthNumber() const
{
	return widthSquareNumber;
}

int TetrisRules::GetHeightSquareNumber() const
{
	return heightSquareNumber;
}

int TetrisRules::GetSquareWidthSize() const
{
	return squareSize * widthSquareNumber;
}

int TetrisRules::GetSquareHeightSize() const
{
	return squareSize * heightSquareNumber;
}