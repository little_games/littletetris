#pragma once
#include<src\Game.h>

class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};

	Actor(class Game* game);

	virtual void update(float deltaTime);

	State GetState();

	void SetState(Actor::State state);

	~Actor();

private:

	Game* mGame;

	State mState;


};

