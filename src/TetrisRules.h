#pragma once
class TetrisRules
{
public:

	//INIT SINGLETON STD FUNCTIONS\\

	static TetrisRules* GetInstances();

	// TetrisRules should not be cloneable.
	TetrisRules(TetrisRules& other) = delete;

	//Singletons should not be assignable.
	void operator=(const TetrisRules&) = delete;


	//CLASS FUNCTION\\

	int GetSquareSize() const;
	int GetSquareWidthNumber() const;
	int GetHeightSquareNumber() const;
	int GetSquareWidthSize() const;
	int GetSquareHeightSize() const;


private:

	TetrisRules();

	static TetrisRules* tetrisRulesInstances;

	int squareSize;
	int widthSquareNumber;
	int heightSquareNumber;

};